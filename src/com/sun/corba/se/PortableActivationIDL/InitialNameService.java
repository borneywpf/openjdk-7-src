package com.sun.corba.se.PortableActivationIDL;


/**
* com/sun/corba/se/PortableActivationIDL/InitialNameService.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from ../../../../src/share/classes/com/sun/corba/se/PortableActivationIDL/activation.idl
* Friday, August 12, 2016 5:24:54 AM UTC
*/


/** Interface used to support binding references in the bootstrap name
    * service.
    */
public interface InitialNameService extends InitialNameServiceOperations, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity 
{
} // interface InitialNameService
